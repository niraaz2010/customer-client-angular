import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';

import {AppComponent} from './app.component';
import {ReactiveFormsModule, FormsModule} from '@angular/forms';
import {DisplayTableComponent} from './display-table/display-table.component';
import {AgGridModule} from 'ag-grid-angular';
import {CustomersComponent} from './customers/customers.component';
import {UpdateComponent} from './update/update.component';
import {AddComponent} from './add/add.component';
import {HttpClientModule} from '@angular/common/http';
import {AppRoutingModule} from './app-routing.module';

@NgModule({
  declarations: [
    AppComponent,
    DisplayTableComponent,
    AppComponent,
    CustomersComponent,
    UpdateComponent,
    AddComponent,
  ],
  imports: [
    BrowserModule,
    ReactiveFormsModule,
    HttpClientModule,
    FormsModule,
    AgGridModule.withComponents([]),
    AppRoutingModule
  ]
  ,
  providers: [],
  bootstrap: [AppComponent]
})

export class AppModule {
}

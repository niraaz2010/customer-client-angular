import {Component, OnInit} from '@angular/core';
import {CustomerService} from '../service/customer.service';
import {ActivatedRoute, Router} from '@angular/router';
import {Customer} from '../customer';

@Component({
  selector: 'app-update',
  templateUrl: './update.component.html',
  styleUrls: ['./update.component.css']
})
export class UpdateComponent implements OnInit {
  customer: Customer;
  id: number;

  constructor(private customerService: CustomerService,
              private route: ActivatedRoute,
              private router: Router) {
  }

  ngOnInit() {
    this.customer = new Customer();
    this.id = this.route.snapshot.params['id'];
    this.getCustomerById(this.id);
  }

  getCustomerById(id) {
    console.log('GET customer by id');
    console.log(id);
    this.customerService.getCustomerById(id).subscribe(
      data => this.customer = data
    );
  }

  handleSave() {
    this.customerService.updateCustomer(this.id, this.customer).subscribe(
      response => {
        this.router.navigate(['customers']);
      }
    );
  }

}

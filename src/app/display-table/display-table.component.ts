import {Component, OnInit} from '@angular/core';
import {ColDef} from 'ag-grid-community';

@Component({
  selector: 'app-display-table',
  templateUrl: './display-table.component.html',
  styleUrls: ['./display-table.component.css']
})
export class DisplayTableComponent implements OnInit {
  columnDefs: ColDef[] = [
    {
      headerName: 'Make',
      field: 'make',
      filter: true,
      sortable: true,
      headerClass: 'bg-green',
      checkboxSelection: true,
      headerCheckboxSelection: true
    },
    {field: 'model'},
    {field: 'price'}
  ];

  rowData = [
    {make: 'Toyota', model: 'Celica', price: 35000},
    {make: 'Ford', model: 'Mondeo', price: 32000},
    {make: 'Porsche', model: 'Boxter', price: 72000}
  ];
  private gridApi: any;
  private gridColumnApi: any;

  constructor() {
  }

  ngOnInit() {
  }

  onGridReady(params: any) {
    this.gridApi = params.api;
    this.gridColumnApi = params.culumnApi;
  }

  getSelectedRows() {
    let selectedRows = this.gridApi.getSelectedRows();
    console.log(selectedRows);
  }
}
